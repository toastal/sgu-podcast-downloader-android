#!/usr/bin/env bash
# Remove this and all above on Android to get the correct shebang
#!/system/bin/env bash
#
# SGU Podcast Downloader Android
#
# SGU’s new site is not accessible for me on my Android phone in 
# any browser and I cannot click the “Download” link. Regardless,
# I don’t want to waste my time going to the site on my mobile
# device only to download the latest podcast.
#
# With BusyBox and a terminal emulator installed, I can use `wget`
# to download an episode by passing it the date of the episode.

if [[ -z $3 ]]; then
	echo
	echo "USAGE: $(basename "$0") <year> <month> <day>"
	echo
	exit 0
fi

install_location="/storage/sdcard1/Skeptics"
sgu_media_url="http://media.libsyn.com/media/skepticsguide"
filename="skepticast$1-$2-$3.mp3"

# Year transformed to 20xx if given a 1 or 2-digit integer
# (won't work after 2099, but let's be real here)
# Month and day transformed to 2 digits
year=$(if [[ $(echo $1 | wc -m) -gt 3 ]]; then echo $1; else printf "20%02d" $1; fi)
month=$(printf "%02" $2)
day=$(printf "%02" $3)

if [[ -d $install_location ]]; then
	if [[ ! -f "$install_location/$filename" ]]; then
		# Download or exit on a non-200 response
		wget -P $install_location "$sgu_media_url/$filename" \
			|| exit 1

		# Change permissions of file so anyone can read and remove
		chmod 777 $install_location/$filename
	else
		echo "File already exists: $filename"
		exit 1
	fi
else
	echo "Not a directory: $install_location"
	exit 1
fi